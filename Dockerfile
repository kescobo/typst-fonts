FROM opensuse/tumbleweed

# -f = auto refresh
# -G = don't do gpg check (breaks with non-interactive)
RUN zypper install -y fontconfig
RUN zypper install -y typst

RUN zypper addrepo -fG https://download.opensuse.org/repositories/M17N:fonts/openSUSE_Tumbleweed/M17N:fonts.repo
RUN zypper install -y liberation-fonts
RUN zypper install -y julia-mono-fonts
RUN zypper install -y fira-code-fonts
RUN zypper install -y fontawesome-fonts
RUN zypper install -y symbols-only-nerd-fonts
