# Typst image with fonts

![Dockerhub link](https://img.shields.io/badge/Dockerhub-repo-blue?logo=docker&link=https%3A%2F%2Fhub.docker.com%2Frepository%2Fdocker%2Fkescobo%2Ftypst-fonts)

- source: OpenSUSE/tumbleweed
- added M17N:Fonts repo
- except for v0.1, tags are `{container-version}-{typst-version} - eg `v0.2-v0.11` refers to typst version v0.11 and repo version v0.2

## Add fonts

Currently, this image includes 

- liberation-fonts
- julia-mono
- fira-code
- fontawesome-fonts
- symbols-only-nerd-fonts

If you'd like add additional fonts, please feel free to open an issue or MR.

## Usage

Tag and push to dockerhub

```sh
$ sudo docker build -t kescobo/typst-fonts:v#.#-v0.## .
$ sudo docker push kescobo/typst-fonts:v#.#-v0.##
```

Use container:

```sh 
$ sudo docker run --rm kescobo/typst-fonts:v0.3-v0.11 typst --version
typst 0.11.0
```

